package org.article19.circulo.next

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import androidx.fragment.app.FragmentActivity

open class BaseActivity: FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  requestWindowFeature(Window.FEATURE_NO_TITLE)
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {

        /**
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            this.window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            this.window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }**/

        return super.onCreateView(name, context, attrs)
    }
}
package org.article19.circulo.next

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Router
import info.guardianproject.keanu.core.service.RemoteImService
import org.article19.circulo.next.notify.StatusBarNotifier
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.timeline.*

class CirculoApp : ImApp () {


    private val mSession: Session?
        get() = matrixSession

    private var mStatusBarNotifier: StatusBarNotifier? = null

    override fun onCreate() {
        super.onCreate()

        router = CirculoRouter ()
        mStatusBarNotifier = StatusBarNotifier(this)

    }


    fun enableCirculoNotifications () {

        //listen to room summary for new events
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN, Membership.INVITE)
        val queryParams = builder.build()

        mSession?.getRoomSummariesLive(queryParams)?.observeForever { roomSums : List<RoomSummary> ->

            for (roomSum in roomSums) {

                if (roomSum.hasNewMessages) {

                    var room = mSession?.getRoom(roomSum.roomId)

                    if (room != null) {
                        val ts = TimelineSettings(10, true)
                        val timeline = room.createTimeline(roomSum.readMarkerId, ts)
                        timeline.start()

                        val te = timeline.getTimelineEventAtIndex(0)
                        if (te != null)
                         handleTimelineEventNotification(te)

                        timeline.dispose()

                        /**
                        timeline.addListener(object : Timeline.Listener {
                            override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {

                                for (event in snapshot)
                                    handleTimelineEventNotification(event)

                            }

                            override fun onTimelineFailure(throwable: Throwable) {
                                // This is what Element does here, too, so might be a good idea for us, too.
                                timeline.restartWithEventId(null)
                            }

                            override fun onNewTimelineEvents(eventIds: List<String>) {
                                for (tEventId in eventIds) {
                                    room.getTimeLineEvent(tEventId)?.let {
                                        if (RemoteImService.filterEvents(it)) handleTimelineEventNotification(
                                            it
                                        )
                                    }
                                }
                            }
                        })

                        timeline.start()**/

                    }


                }

            }
        }

        // check room timeline for new events

        // show custom notification based on
           /// is reply to my status
           /// is urgent / "help now" status
    }

    fun handleTimelineEventNotification(te: TimelineEvent) {
        // Ignore, when our user sent this.
        if (mSession?.myUserId == te.senderInfo.userId) return

        // Ignore, when notifications for room are disabled.

        val mc = te.getLastMessageContent()
        if (mc is MessageWithAttachmentContent) RemoteImService.downloadMedia(mSession, mc)

        mStatusBarNotifier?.notifyChat(te)
    }



}


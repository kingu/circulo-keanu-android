package org.article19.circulo.next

import android.content.Context
import android.content.Intent
import info.guardianproject.keanu.core.MainActivity
import info.guardianproject.keanu.core.Router

class CirculoRouter : Router() {

    companion object {
        val instance = CirculoRouter()
    }

    private val circuloOnboardingClass
        get() = org.article19.circulo.next.onboarding.LauncherActivity::class.java

    private val circuloMainClass
        get() = org.article19.circulo.next.main.MainActivity::class.java

    override fun onboarding(context: Context): Intent {
        return Intent(context, circuloOnboardingClass)

    }

    override fun main(context: Context, inviteUserId: String?, joinRoomId: String?,
                      openRoomId: String?, isFirstTime: Boolean?, preselectTab: Int?): Intent
    {
        val i = Intent(context, circuloMainClass)

        if (inviteUserId != null) {
            i.putExtra(MainActivity.EXTRA_INVITE_USER_ID, inviteUserId)
        }

        if (joinRoomId != null) {
            i.putExtra(MainActivity.EXTRA_JOIN_ROOM_ID, joinRoomId)
        }

        if (openRoomId != null) {
            i.putExtra(MainActivity.EXTRA_OPEN_ROOM_ID, openRoomId)
        }

        if (isFirstTime != null) {
            i.putExtra(MainActivity.EXTRA_IS_FIRST_TIME, isFirstTime)
        }

        if (preselectTab != null) {
            i.putExtra(MainActivity.EXTRA_PRESELECT_TAB, preselectTab)
        }

        return i
    }
}
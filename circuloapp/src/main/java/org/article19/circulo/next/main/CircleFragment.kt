package org.article19.circulo.next.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import org.article19.circulo.next.R

class CircleFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_circle, container, false)
        rootView.findViewById<Button>(R.id.btn_join_circle).setOnClickListener {
            startActivity(Intent(context, JoinCircleActivity::class.java))
        }
        rootView.findViewById<Button>(R.id.btn_create_circle).setOnClickListener {
            startActivityForResult(Intent(context, CreateCircleActivity::class.java), 1)
        }

        return rootView

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            (activity as MainActivity).replaceCurrentFragment(OurCircleFragment(), false)
        }
    }


    private fun showJoinCircle() {
        val joinCircleString = getString(R.string.join_a_circle)
        val joinCircleMessageString =
            getString(R.string.join_circle_invite_message, "Travel People")
        val notNowString = getString(R.string.not_now)
        val joinString = R.string.join
        context?.let {
            InfoSheet().show(it) {
                title(joinCircleString)
                content(joinCircleMessageString)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(joinString) {

                }

            }
        }
    }


}
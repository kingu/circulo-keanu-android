package org.article19.circulo.next.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.Config
import org.article19.circulo.next.R
import org.article19.circulo.next.main.circleinfo.CircleInfoActivity
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams

class JoinCircleActivity : BaseActivity() {


    private val mApp: ImApp?
        get() = getApplication() as? ImApp

    private var mCurrentRoom: RoomSummary? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_circle)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<TextView>(R.id.tv_back_text).visibility = View.GONE
        toolbar.findViewById<TextView>(R.id.tv_action).visibility = View.VISIBLE
        toolbar.findViewById<TextView>(R.id.tv_action).setOnClickListener {
            finish()
        }

        val editTextCircleLink = findViewById<EditText>(R.id.edit_text_circle_link)

        if (intent?.dataString?.isNotEmpty() == true)
            editTextCircleLink.setText(intent?.dataString)
        if (intent?.data != null)
            editTextCircleLink.setText(intent?.data?.toString())

        findViewById<Button>(R.id.btn_next).setOnClickListener {
            Config.ENABLE_EMPTY_STATE = false

            joinRoom();

        }

        queryCircles()
    }

    fun queryCircles () {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        var queryParams = builder.build()

        var listRooms = mApp?.matrixSession?.getRoomSummaries(queryParams)
        if (listRooms?.isEmpty() == false)
            mCurrentRoom = listRooms?.get(0)
    }

    fun joinRoom () {
     //   https://encirculo.org/i/#!qogyWZmbSHNpecxUsV:neo.keanu.im

        var roomId = findViewById<EditText>(R.id.edit_text_circle_link).text.toString()

        if (roomId.isNotEmpty()) {
            mCoroutineScope.launch {

                roomId = roomId.split("#")[1]

                if (mCurrentRoom == null) {
                    roomId = mApp?.matrixSession?.joinRoom(roomId).toString()
                    finish()
                }
                else
                {
                    showLeaveCircleAlert(roomId)
                }


            }
        }

    }

    private fun showLeaveCircleAlert(newRoomId: String) {

        //val newRoom = mApp?.matrixSession?.getRoom(newRoomId)
        //val newRoomDisplayName = newRoom?.roomSummary()?.displayName

        val leaveCircleString =
            getString(R.string.leave_circle_title, mCurrentRoom?.displayName)
        val leaveCircleMessage = getString(R.string.leave_circle_message)
        val notNowString = getString(R.string.not_now)
        val yesString = getString(R.string.Yes)
        let {
            InfoSheet().show(it) {
                title(leaveCircleString)
                content(leaveCircleMessage)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(yesString) {

                    mCoroutineScope.launch {

                        //leave current room
                        mApp?.matrixSession?.getRoom(mCurrentRoom?.roomId!!)?.leave("")

                        //join new room
                        mApp?.matrixSession?.joinRoom(newRoomId).toString()
                    }

                    finish()
                }

            }
        }
    }

}
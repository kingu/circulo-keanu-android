package org.article19.circulo.next.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import timber.log.Timber

class MainActivity : BaseActivity(), Observer<List<RoomSummary>> {

    private lateinit var fragmentNow: Fragment
    private lateinit var fragmentCircle: Fragment
    private lateinit var fragmentProfile: Fragment
    private lateinit var fragmentManager: FragmentManager
    private lateinit var currentFragment: Fragment
    private lateinit var mNavigationView: BottomNavigationView

    var inACircle : Boolean = false

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        mNavigationView = findViewById(R.id.bottom_bar)
        mNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.now -> {
                    if (!this::fragmentNow.isInitialized) {
                        fragmentNow = NowFragment()
                    }

                    if (currentFragment !is NowFragment) {
                        fragmentManager.beginTransaction().replace(R.id.layout_main_container, fragmentNow).commit()
                        currentFragment = fragmentNow
                    }

                    true
                }

                R.id.circle -> {
                    if (!this::fragmentCircle.isInitialized) {

                        fragmentCircle = if (!inACircle)
                            CircleFragment()
                        else
                            OurCircleFragment()
                    }

                    if (currentFragment !is CircleFragment || currentFragment !is OurCircleFragment) {
                        fragmentManager.beginTransaction().replace(R.id.layout_main_container, fragmentCircle).commit()
                        currentFragment = fragmentCircle
                    }

                    true
                }

                R.id.profile -> {
                    if (!this::fragmentProfile.isInitialized) {
                        fragmentProfile = ProfileFragment()
                    }

                    if (currentFragment !is ProfileFragment) {
                        fragmentManager.beginTransaction().replace(R.id.layout_main_container, fragmentProfile).commit()
                        currentFragment = fragmentProfile
                    }

                    true
                }

                else -> false
            }
        }

        queryCircles()

        initFragments()

    }

    private fun initFragments() {
        fragmentNow = NowFragment()
        fragmentManager = supportFragmentManager
        currentFragment = fragmentNow

//        fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentProfile).hide(fragmentProfile).commit()
//        fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentCircle).hide(fragmentProfile).commit()
        fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentNow).commit()
    }

    fun replaceCurrentFragment(newFragment: Fragment, addToBackStack: Boolean) {
        fragmentManager.beginTransaction().replace(R.id.layout_main_container, newFragment).let {
            if (addToBackStack) {
                it.addToBackStack(null)
            }
            it.commit()
        }
        currentFragment = newFragment
    }

    fun inACircle () : Boolean
    {
        return inACircle
    }

    fun queryCircles () {

        mApp?.matrixSession?.startSync(true)
        mApp?.matrixSession?.startAutomaticBackgroundSync(5L,5L)


        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        var queryParams = builder.build()

        mApp?.matrixSession?.getRoomSummariesLive(queryParams)?.observe(this, this)

        mApp?.matrixSession?.getSyncStateLive()?.observe(this, Observer {

            Timber.d("SyncState: " + it)
        })

        mApp?.matrixSession?.getSyncStatusLive()?.observe(this, Observer {


            Timber.d("SyncStatus: " + it)

        })


    }

    override fun onChanged(roomSummaries: List<RoomSummary>?) {

        var newInACircle = roomSummaries?.isEmpty() == false

        if (!inACircle)
            fragmentCircle = CircleFragment()
        else
            fragmentCircle = OurCircleFragment()

        if (findViewById<BottomNavigationView>(R.id.bottom_bar).selectedItemId == 1) {

            if (this::fragmentCircle.isInitialized) {
                fragmentManager.beginTransaction()
                    .replace(R.id.layout_main_container, fragmentCircle).commit()
            }
        }

        inACircle = newInACircle

    }

    override fun onResume() {
        super.onResume()
        queryCircles()

        mApp?.enableCirculoNotifications()
    }

    fun hideNavigationView() {
        mNavigationView.visibility = View.GONE
    }

    fun showNavigationView() {
        mNavigationView.visibility = View.VISIBLE
    }
}
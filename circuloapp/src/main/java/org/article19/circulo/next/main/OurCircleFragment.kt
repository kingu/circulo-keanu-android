package org.article19.circulo.next.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.main.circleinfo.CircleInfoActivity
import org.article19.circulo.next.main.circleinfo.CircleMember
import org.article19.circulo.next.main.circleinfo.CircleSummary
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.powerlevels.PowerLevelsHelper
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import kotlin.random.Random

class OurCircleFragment : Fragment() {

    private val mRandomAvatars = listOf(
        R.drawable.ic_avatar_ana,
        R.drawable.ic_avatar_chloe,
        R.drawable.ic_avatar_juana,
        R.drawable.ic_avatar_marta,
        R.drawable.ic_avatar_mariel
    )

    private val TAG = "OurCircleFragment"

    private var mRoom: Room? = null
    private var mMembers = ArrayList<GroupMember>()
    private var mPlHelper: PowerLevelsHelper? = null
    private var mRoomSummary: RoomSummary? = null

    private val mApp: ImApp?
        get() = activity?.application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mYou = GroupMember()
    private var mRootView: View? = null
    private var mCircleMembers = mutableListOf<CircleMember>()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    companion object {
        const val BUNDLE_EXTRA_CIRCLE_SUMMARY = "bundle_extra_circle_summary"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_our_circle, container, false)

        inflateMemberData(rootView)

        rootView.findViewById<TextView>(R.id.tv_info).setOnClickListener {
//          open group info view
            val circleInfoIntent = Intent(activity, CircleInfoActivity::class.java)
            circleInfoIntent.putExtra(BUNDLE_EXTRA_CIRCLE_SUMMARY, CircleSummary(mRoomSummary?.roomId ?:"", mRoomSummary?.displayName ?: "", mCircleMembers,
            mRoomSummary?.isPublic ?: false))
            activity?.startActivity(circleInfoIntent)
        }

        mRootView = rootView



        return rootView
    }

    override fun onResume() {
        super.onResume()

        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        val queryParams = builder.build()

        mSession?.getRoomSummariesLive(queryParams)?.observe(this, Observer { itRooms ->

            itRooms.firstOrNull()?.let {

                //Circulo is always meant to be in just ONE room, so we can just get that room
                mRoomSummary = it
                mRoom = mApp?.matrixSession?.getRoom(mRoomSummary!!.roomId)

                updateCircleName()

                updateMembers()
            }
        })
        
    }

    private fun updateCircleName () {
        mRootView?.findViewById<TextView>(R.id.tv_headline)?.text = mRoom?.roomSummary()?.displayNameWorkaround
    }

    private fun openInviteView () {
        //startActivity(Intent(activity, CirculoRouter.instance.addFriendClass))

        val inviteUrl = "https://encirculo.org/i/#" + mRoom?.roomId

        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, inviteUrl)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)

    }

    private fun inflateMemberData(rootView: View) {

        val layoutMemberOwner = rootView.findViewById<LinearLayout>(R.id.btn_add_6)
        inflatePersonData(layoutMemberOwner, 0)

        val layoutFirstMember = rootView.findViewById<LinearLayout>(R.id.btn_add_1)
        inflatePersonData(layoutFirstMember,1)

        val layoutSecondMember = rootView.findViewById<LinearLayout>(R.id.btn_add_2)
        inflatePersonData(layoutSecondMember,2)

        val layoutThirdMember = rootView.findViewById<LinearLayout>(R.id.btn_add_3)
        inflatePersonData(layoutThirdMember,3)

        val layoutFourthMember = rootView.findViewById<LinearLayout>(R.id.btn_add_4)
        inflatePersonData(layoutFourthMember,4)

        val layoutFifthMember = rootView.findViewById<LinearLayout>(R.id.btn_add_5)
        inflatePersonData(layoutFifthMember,5)

    }

    private fun inflatePersonData(layoutMember: LinearLayout, memberIndex: Int) {

        val ivAvatar = layoutMember.findViewById<RoundRectCornerImageView>(R.id.iv_avatar)
        val layoutAvatar = layoutMember.findViewById<RelativeLayout>(R.id.layout_avatar)

        if (memberIndex < mMembers.size) {
            layoutAvatar.background = null
            val tvMemberName = layoutMember.findViewById<TextView>(R.id.tv_member_name)
            ivAvatar.visibility = View.VISIBLE

            mCoroutineScope.launch {
                val user = mMembers[memberIndex].username?.let { mApp?.matrixSession?.getUser(it) }
                if (user?.avatarUrl?.isNotEmpty() == true) {

                    mSession?.contentUrlResolver()?.resolveThumbnail(user.avatarUrl, 59, 59, ContentUrlResolver.ThumbnailMethod.SCALE)
                        ?.let {
                            withContext(Dispatchers.Main) {
                                GlideUtils.loadImageFromUri(activity, Uri.parse(it), ivAvatar, true)
                            }
                        }

                }
                else
                {
                    withContext(Dispatchers.Main) {
                        ivAvatar.setImageResource(mRandomAvatars[Random.nextInt(5)])
                    }
                }
            }

            if (mSession?.myUserId?.equals(mMembers[memberIndex].username) == true) {
                tvMemberName.text = getString(R.string.me_title)
            } else {
                tvMemberName.text = mMembers[memberIndex].nickname
            }
            tvMemberName.visibility = View.VISIBLE

            layoutMember.findViewById<ImageView>(R.id.iv_add).visibility = View.GONE
            layoutMember.setOnClickListener {
                //TODO: Should we open person's profile?
            }
        } else {

            ivAvatar.visibility = View.GONE
            layoutAvatar.setBackgroundResource(R.drawable.bg_button_add_to_circle)
            layoutMember.findViewById<ImageView>(R.id.iv_add).visibility = View.VISIBLE

            layoutMember.setOnClickListener {
                openInviteView()
            }
        }

    }

    private fun updateMembers() {
        updatePowerLevels(
            mRoom?.getStateEvent(
                EventType.STATE_ROOM_POWER_LEVELS,
                QueryStringValue.NoCondition
            )
        )

        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()

        mRoom?.getRoomMembersLive(query)?.observe(this) {

            updateRoomMembers(it)
        }


    }

    private fun updateRoomMembers(roomMemberSummaries: List<RoomMemberSummary>) {
        mMembers.clear()

        for (roomMember in roomMemberSummaries) {

            val member = GroupMember(
                username = roomMember?.userId,
                nickname = roomMember?.displayName,
                role = mPlHelper?.getUserRole(roomMember?.userId) ?: Role.Default,
                affiliation = roomMember?.membership.name,
                avatarUrl = roomMember?.avatarUrl
            )

            if (mSession?.myUserId?.contentEquals(member.username) == true) {
                mYou = member
            }

            val canGrantAdmin = canGrantAdmin(member)
            if (canGrantAdmin) {
                mMembers.add(0, member)
            } else {
                mMembers.add(member)
            }

            val circleMember = CircleMember(member.nickname, roomMember?.userId, member.avatarUrl, canGrantAdmin, 0L)
            mCircleMembers.add(circleMember)

        }

        mRootView?.let { inflateMemberData(it) }

    }

    private fun canGrantAdmin(granter: GroupMember?): Boolean {
        return granter?.role is Role.Admin || granter?.role is Role.Moderator
    }

    private fun updatePowerLevels(eventPowerLevels: Event?) {
        if (eventPowerLevels == null) return

        val mapLevels = eventPowerLevels.getClearContent()

        /**
        MoshiProvider.providesMoshi().adapter(PowerLevelsContent::class.java).fromJsonValue(mapLevels)?.let {
            mPlHelper = PowerLevelsHelper(it)
        }**/
    }

    inner class GroupMember(
        var username: String? = null,
        nickname: String? = null,
        var role: Role = Role.Default,
        var affiliation: String? = null,
        var avatarUrl: String? = null,
        var online: Boolean = false
    ) {
        private var _nickname: String? = nickname

        var nickname: String?
            get() = if (_nickname.isNullOrBlank()) username else _nickname
            set(value) {
                _nickname = value
            }

        override fun toString(): String {
            return "GroupMember(username=$username, role=$role, affiliation=$affiliation, avatarUrl=$avatarUrl, online=$online, _nickname=$_nickname)"
        }
    }

    private fun RoomSummary.toString(): String {
        return "RoomSummary(roomId='$roomId', displayName='$displayName', name='$name', topic='$topic', avatarUrl='$avatarUrl', canonicalAlias=$canonicalAlias, aliases=$aliases, joinRules=$joinRules, isDirect=$isDirect, directUserId=$directUserId, joinedMembersCount=$joinedMembersCount, invitedMembersCount=$invitedMembersCount, latestPreviewableEvent=$latestPreviewableEvent, otherMemberIds=$otherMemberIds, notificationCount=$notificationCount, highlightCount=$highlightCount, hasUnreadMessages=$hasUnreadMessages, tags=$tags, membership=$membership, versioningState=$versioningState, readMarkerId=$readMarkerId, userDrafts=$userDrafts, isEncrypted=$isEncrypted, encryptionEventTs=$encryptionEventTs, typingUsers=$typingUsers, inviterId=$inviterId, breadcrumbsIndex=$breadcrumbsIndex, roomEncryptionTrustLevel=$roomEncryptionTrustLevel, hasFailedSending=$hasFailedSending, roomType=$roomType, spaceParents=$spaceParents, spaceChildren=$spaceChildren, flattenParentIds=$flattenParentIds)"
    }
}
package org.article19.circulo.next.main

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.profile.EditProfileFragment
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary

class ProfileFragment: Fragment() {

    private val TAG = "ProfileFragment"

    companion object {
        const val BUNDLE_EXTRA_DISPLAY_NAME = "bundle_extra_display_name"
        const val BUNDLE_EXTRA_AVATAR_URL = "bundle_extra_avatar_url"
//        const val REQUEST_KEY_AVATAR_CHANGE = "request_key_avatar_change"
//        const val BUNDLE_KEY_IS_AVATAR_CHANGE = "bundle_key_is_avatar_change"
    }

    private val mApp: ImApp?
        get() = activity?.getApplication() as? ImApp


    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mRoomSummary: RoomSummary? = null


    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private lateinit var mTvUserName: TextView
    private lateinit var mIvAvatar: RoundRectCornerImageView
    private var mAvatarUrl: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)
        mTvUserName = rootView.findViewById(R.id.tv_username)
        mIvAvatar = rootView.findViewById(R.id.iv_avatar)

        val username = mApp?.matrixSession?.myUserId
        setDisplayName(username.toString().split(":")[0])

        rootView.findViewById<View>(R.id.layout_item_account).setOnClickListener {

            startActivity(mApp?.router?.devices(requireActivity()))

        }

        rootView.findViewById<View>(R.id.layout_item_physical_security).setOnClickListener {

       //     startActivity(mApp?.router?.lockScreen(requireActivity()))

        }

        rootView.findViewById<View>(R.id.layout_item_settings).setOnClickListener {

       //     startActivity(mApp?.router?.settings(requireActivity()))

        }

        rootView.findViewById<View>(R.id.layout_item_notifications).setOnClickListener {

    //        activity?.startActivity(mApp?.router?.groupDisplay(requireActivity(), mRoomSummary?.roomId))

        }

        reloadUserInfo()

        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        val queryParams = builder.build()

        mSession?.getRoomSummariesLive(queryParams)?.observe (requireActivity(), Observer { itRooms ->

            itRooms.firstOrNull()?.let {

                //Circulo is always meant to be in just ONE room, so we can just get that room
                mRoomSummary = it
            }
        })

        rootView.findViewById<Button>(R.id.btn_edit_profile).setOnClickListener {
            val args = Bundle()
            args.putString(BUNDLE_EXTRA_DISPLAY_NAME, mTvUserName.text.toString())
            args.putString(BUNDLE_EXTRA_AVATAR_URL, mAvatarUrl)
            val editProfileFragment = EditProfileFragment()
            editProfileFragment.arguments = args
            (activity as MainActivity).replaceCurrentFragment(editProfileFragment, true)
        }

        //Listen to possible avatar changes when coming back here from EditProfileFragment
//        setFragmentResultListener(REQUEST_KEY_AVATAR_CHANGE) { requestKey, bundle ->
//            val shouldRefreshAvatar = bundle.getBoolean(BUNDLE_KEY_IS_AVATAR_CHANGE)
//            if (shouldRefreshAvatar) {
//                reloadUserInfo()
//            }
//        }

        return rootView
    }

    private fun setDisplayName (display : String)
    {
        activity?.runOnUiThread {
            mTvUserName.text = display
        }
    }

    private fun reloadUserInfo() {
        mCoroutineScope.launch {
            val user = mApp?.matrixSession?.myUserId?.let { mApp?.matrixSession?.getUser(it) }
            if (user?.displayName?.isNotEmpty() == true) {
                setDisplayName(user.displayName!!)
                mAvatarUrl = user.avatarUrl

                mSession?.contentUrlResolver()?.resolveThumbnail(mAvatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE)
                    ?.let {
                        withContext(Dispatchers.Main) {
                            GlideUtils.loadImageFromUri(activity, Uri.parse(it), mIvAvatar, true)
                        }
                    }

            }

            val userProfile = mApp?.matrixSession?.myUserId.let { mApp?.matrixSession?.getProfile(it!!) }
            userProfile?.forEach {
                Log.w(TAG, "User info key: " + it.key + ". Value: " + it.value)
            }
        }
    }
}
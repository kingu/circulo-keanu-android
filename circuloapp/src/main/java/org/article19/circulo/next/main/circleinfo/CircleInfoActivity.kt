package org.article19.circulo.next.main.circleinfo

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanuapp.ui.qr.QrDisplayActivity
import info.guardianproject.keanuapp.ui.qr.QrScanActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.Config
import org.article19.circulo.next.R
import org.article19.circulo.next.main.OurCircleFragment
import org.article19.circulo.next.main.OurCircleFragment.Companion.BUNDLE_EXTRA_CIRCLE_SUMMARY
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary
import org.matrix.android.sdk.api.session.room.powerlevels.Role

class CircleInfoActivity : BaseActivity() {

    private lateinit var mTvCircleType: TextView
    private lateinit var mTvCircleTypeDescription: TextView

    private lateinit var mTvShareLink: TextView
    private lateinit var mTvShowQr: TextView
    private lateinit var mViewSeparatorShareLink: View
    private lateinit var mViewSeparatorShowQr: View

    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mRoom: Room? = null
    private var mCircleSummary: CircleSummary? = null

    private val memberList = mutableListOf<CircleMember>()
    private val memberAdapter = CircleMemberAdapter(memberList)

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_circle_info)

        mCircleSummary = intent.getParcelableExtra<CircleSummary>(BUNDLE_EXTRA_CIRCLE_SUMMARY)

        if (mCircleSummary != null) {

            if (mCircleSummary?.roomId != null)
                mRoom = mSession?.getRoom(mCircleSummary?.roomId!!)

            val toolbar = findViewById<Toolbar>(R.id.toolbar)
            toolbar.findViewById<TextView>(R.id.tv_action).visibility = View.GONE
            toolbar.findViewById<TextView>(R.id.tv_title).text = getString(R.string.circle_info)
            toolbar.findViewById<TextView>(R.id.tv_back_text).setOnClickListener {
                finish()
            }

            mTvShareLink = findViewById(R.id.tv_share_link)
            mTvShowQr = findViewById(R.id.tv_show_qr)
            mViewSeparatorShareLink = findViewById(R.id.view_separator_4)
            mViewSeparatorShowQr = findViewById(R.id.view_separator_5)

            mTvShareLink.setOnClickListener {
                openInviteView()
            }

            mTvShowQr.setOnClickListener {
                openQrView()

            }

            findViewById<View>(R.id.tv_leave_circle).setOnClickListener {
                showLeaveCircleAlert()
            }

            val recyclerViewMemberList = findViewById<RecyclerView>(R.id.recycler_view_members)
            val layoutMgr = LinearLayoutManager(this)
            recyclerViewMemberList.layoutManager = layoutMgr

            recyclerViewMemberList.adapter = memberAdapter

            findViewById<TextView>(R.id.tv_circle_name).text = mCircleSummary?.name
            val isPublicCircle = mCircleSummary?.isPublic ?: false
            mTvCircleType = findViewById(R.id.tv_circle_type)
            mTvCircleTypeDescription = findViewById(R.id.tv_circle_type_description)

            initCircleTypeLayout(isPublicCircle)

            updateMembers()
        }
        else
            finish()
    }

    private fun updateMembers() {


        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()

        //mRoom?.let { updateRoomMembers(it.getRoomMembers(query)) }

        mRoom?.getRoomMembersLive(query)?.observe(this) {

            updateRoomMembers(it)


        }


    }

    private fun updateRoomMembers(roomMemberSummaries: List<RoomMemberSummary>) {

        memberList.clear()

        for (roomMember in roomMemberSummaries) {

            val displayName = roomMember.displayName
            val userId = roomMember.userId
            val avatarUrl = roomMember.avatarUrl
            val canGrantAdmin = false;//canGrantAdmin(member)

            val circleMember = CircleMember(displayName, userId, avatarUrl, canGrantAdmin, 0L)
            memberList.add(circleMember)
        }


        memberAdapter.notifyDataSetChanged()
    }

    private fun canGrantAdmin(granter: OurCircleFragment.GroupMember?): Boolean {
        return granter?.role is Role.Admin || granter?.role is Role.Moderator
    }

    private fun initCircleTypeLayout(isPublic: Boolean) {
        mTvShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE
        mTvShowQr.visibility = if (isPublic) View.VISIBLE else View.GONE
        mViewSeparatorShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE
        mViewSeparatorShareLink.visibility = if (isPublic) View.VISIBLE else View.GONE

        mTvCircleType.text = if (isPublic) getString(R.string.open) else getString(R.string.closed)
        mTvCircleTypeDescription.text = if (isPublic) getString(R.string.anyone_with_a_link_can_join)
        else getString(R.string.only_people_added_in_your_circle)
    }

    private fun showLeaveCircleAlert() {
        val leaveCircleString =
            getString(R.string.leave_circle_direct_title, mCircleSummary?.name)
        val leaveCircleMessage = ""
        val notNowString = getString(R.string.not_now)
        val yesString = getString(R.string.Yes)
        let {
            InfoSheet().show(it) {
                title(leaveCircleString)
                content(leaveCircleMessage)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(yesString) {

                    mCoroutineScope.launch {
                        mRoom?.leave("")
                    }

                }

            }
        }
    }

    private fun openInviteView () {
        //startActivity(Intent(activity, CirculoRouter.instance.addFriendClass))

        val inviteUrl = "https://encirculo.org/i/#" + mRoom?.roomId

        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, inviteUrl)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)

    }

    private fun openQrView () {
        //startActivity(Intent(activity, CirculoRouter.instance.addFriendClass))

        val inviteUrl = "https://encirculo.org/i/#" + mRoom?.roomId
        val mimeType = "text/plain"

        val i = Intent(this, QrDisplayActivity::class.java)

        if (inviteUrl != null) {
            i.putExtra(Intent.EXTRA_TEXT, inviteUrl)
        }

        if (mimeType != null) {
            i.setTypeAndNormalize(mimeType)
        }

        startActivity(i)

    }

    fun getSession(): Session? {
        return mSession
    }

    fun getCoroutineScope(): CoroutineScope {
        return mCoroutineScope
    }
}
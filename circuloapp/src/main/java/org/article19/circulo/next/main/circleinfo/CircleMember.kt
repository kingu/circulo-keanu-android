package org.article19.circulo.next.main.circleinfo

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CircleMember(
    val name: String?, val userId: String, val avatarUrl: String?, val isAdmin: Boolean, val lastActiveTime: Long
) : Parcelable {

    override fun toString(): String {
        return "CircleMember(name='$name', userId='$userId', avatarUrl='$avatarUrl', isAdmin=$isAdmin, lastActiveTime=$lastActiveTime)"
    }
}

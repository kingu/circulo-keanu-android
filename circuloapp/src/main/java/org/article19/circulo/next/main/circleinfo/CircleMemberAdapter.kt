package org.article19.circulo.next.main.circleinfo

import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.GlideUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.matrix.android.sdk.api.session.content.ContentUrlResolver

class CircleMemberAdapter(private val memberList: MutableList<CircleMember>) :
    RecyclerView.Adapter<CircleMemberAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_circle_member, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentMember = memberList[position]
        holder.tvName.text = currentMember.name

        if (TextUtils.isEmpty(currentMember.avatarUrl)) {
            holder.ivAvatar.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_avatar_chloe))
        } else {
            if (context is CircleInfoActivity) {
                val parentActivity = context as CircleInfoActivity
                parentActivity.getCoroutineScope().launch {
                    parentActivity.getSession()?.contentUrlResolver()?.resolveThumbnail(currentMember.avatarUrl, 59, 59, ContentUrlResolver.ThumbnailMethod.SCALE)
                        ?.let {
                            withContext(Dispatchers.Main) {
                                GlideUtils.loadImageFromUri(context, Uri.parse(it), holder.ivAvatar, true)
                            }
                        }
                }
            }
        }

        if (currentMember.isAdmin) {
            holder.ivAdminBadge.visibility = View.VISIBLE
        }

    }

    override fun getItemCount(): Int {
        return memberList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivAvatar: RoundRectCornerImageView = view.findViewById(R.id.iv_avatar)
        val ivAdminBadge: ImageView = view.findViewById(R.id.iv_admin_badge)
        val tvName: TextView = view.findViewById(R.id.tv_name)
        val tvLastActiveTime: TextView = view.findViewById(R.id.tv_last_active_time)
    }

    fun updateList(newMemberList : MutableList<CircleMember>) {
        memberList.clear()
        memberList.addAll(newMemberList)
    }
}
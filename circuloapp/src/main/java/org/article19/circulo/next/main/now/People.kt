package org.article19.circulo.next.main.now

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class People(
    val roomId: String, val eventId: String, val userId: String, val name: String, val status: Int, val commentCount: Int,
    val isUnread: Boolean, val isUrgent: Boolean, val lastCommentTime: Long
) : Parcelable {
    override fun toString(): String {
        return "People(name='$name', status=$status, commentCount=$commentCount, isUnread=$isUnread, isUrgent=$isUrgent, lastCommentTime=$lastCommentTime)"
    }
}

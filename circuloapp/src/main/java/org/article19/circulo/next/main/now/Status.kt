package org.article19.circulo.next.main.now

object Status {
    const val SAFE = 0
    const val UNCERTAIN = 1
    const val NOT_SAFE = 2

    const val SAFE_STRING = "#safe"
    const val NOT_SAFE_STRING = "#notsafe"
    const val UNCERTAIN_STRING = "#uncertain"

    const val URGENT_STRING = "#urgent"

}
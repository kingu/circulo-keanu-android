package org.article19.circulo.next.main.updatestatus

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.article19.circulo.next.R

class QuickResponseAdapter(var responseList: List<String>) :
    RecyclerView.Adapter<QuickResponseAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_quick_response_status, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentResponse = responseList[position]
        holder.tvResponse.text = currentResponse


        holder.tvResponse.setOnClickListener {
            if (context is UpdateStatusActivity) {
                (context as UpdateStatusActivity).updateQuickResponseList(currentResponse)
            }
        }
    }

    override fun getItemCount(): Int {
        return responseList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvResponse: TextView = view.findViewById(R.id.tv_response)
    }

    fun replaceData(newResponseList: List<String>) {
        responseList = newResponseList
        notifyDataSetChanged()
    }
}
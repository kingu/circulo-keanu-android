package org.article19.circulo.next.main.updatestatus

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.quickresponse.QuickResponseLongClickListener
import info.guardianproject.keanu.core.ui.widgets.StatusItemHolder
import info.guardianproject.keanu.core.ui.widgets.VerificationRequestItemHolder
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanu.core.util.extensions.getRelatedEventId
import info.guardianproject.keanu.core.util.extensions.isReaction
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.StatusItemBinding
import info.guardianproject.keanuapp.databinding.VerificationRequestItemBinding
import info.guardianproject.keanuapp.ui.conversation.QuickReaction
import info.guardianproject.keanuapp.ui.widgets.MediaInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.databinding.StatusReplyBaseBinding
import org.matrix.android.sdk.api.MatrixCallback
import org.matrix.android.sdk.api.session.content.ContentUploadStateTracker
import org.matrix.android.sdk.api.session.events.model.*
import org.matrix.android.sdk.api.session.file.FileService
import org.matrix.android.sdk.api.session.room.model.message.*
import org.matrix.android.sdk.api.session.room.timeline.*
import org.matrix.android.sdk.internal.crypto.MXEventDecryptionResult
import org.matrix.android.sdk.internal.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.internal.crypto.model.rest.EncryptedFileInfo
import java.util.*
import kotlin.collections.HashMap

open class ResponseMessageAdapter(
    private val mActivity: StatusDetailActivity,
    roomId: String,
    parentEventId: String,
    private val mView: RecyclerView
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    ContentUploadStateTracker.UpdateListener, ResponseViewHolder.OnImageClickedListener {

    var lastSelectedView: View? = null
        private set

    private val mApp
        get() = mActivity.application as? ImApp

    private val mSession
        get() = mApp?.matrixSession

    private val mRoom = mSession?.getRoom(roomId)

    private val mTimeline = mRoom?.createTimeline(null, TimelineSettings(50, true))

    private var mTimelineIds = mutableListOf<String>()

    private var mIsBackPage = false

    private val mHandler = Handler(Looper.getMainLooper())

    private var mShowProgressOnPosition: Int? = null
    private var mProgressCurrent: Long = 0
    private var mProgressTotal: Long = 0

    private val mDownloadedDecryptedFiles = HashMap<Int, MediaInfo>()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mView))
    }

    private val allowedRoomTypes = listOf(
        EventType.MESSAGE,
        EventType.STICKER,
        EventType.REACTION,
        EventType.TYPING,
        EventType.RECEIPT)

    /**
     * Standard filter for event lists. Will drop all events, which are
     *
     * - a redaction (The closest thing to a deletion, we can have in Matrix.)
     * - not of any type listed in `ALLOWED_ROOM_TYPES`.
     * - an edition of another event (Matrix SDK will show the edition on the original event, typically, anyway.)
     *
     * You would typically use this as predicate in a [Iterable.filter] call in a [Timeline.Listener]!
     *
     * @return true, if ok, false if it should be discarded.
     */
    val filterEvents = { te: TimelineEvent ->
        // This is ordered in perceived weight of executed code!
        !te.root.isRedacted()
                && te.root.getClearType().let { t -> allowedRoomTypes.any { it == t } }
                && !te.isEdition()
                && te.isReply()
               && te.getRelationContent()?.inReplyTo?.eventId?.equals(parentEventId) == true
    }

    private val tListener = object : Timeline.Listener {

        override fun onTimelineUpdated(snapshot: List<TimelineEvent>) {
            mHandler.post {
                val oldIds = mTimelineIds

                mTimelineIds = snapshot
                    .filter(filterEvents)
                    .map { it.eventId }
                    .reversed() // `snapshot` is newest first, but `RecyclerView` is newest at bottom. -> Reverse!
                    .toMutableList()

                /**
                for (i in mTimelineIds.size-1 downTo 0) {
                    val te = getTimelineEvent(i)
                    if (te?.root?.isRedacted() == true)
                    {
                        mTimelineIds.removeAt(i)
                    }
                    else if (te?.isReply() == false)
                    {
                        mTimelineIds.removeAt(i)
                    }
                    else {
                        val relContent = te?.getRelationContent()
                        val replyEventId = relContent?.inReplyTo?.eventId
                        if (replyEventId?.equals(parentEventId) == false) {
                            mTimelineIds.removeAt(i)
                        }
                    }
                }**/

                while (mTimelineIds.size < oldIds.size)
                    mTimelineIds.add("-1")

                // First, remove all old events, which aren't available in the new list anymore.
                for (i in oldIds.indices) {
                    if (mTimelineIds.indexOf(oldIds[i]) < 0) {
                        notifyItemRemoved(i)
                    }
                }

                // Then, add and update the others, if needed.
                for (i in mTimelineIds.indices) {
                    val oldI = oldIds.indexOf(mTimelineIds[i])

                    if (oldI < 0) {
                        val te = getTimelineEvent(i)

                        // When new reactions come in, their related event needs to get updated.
                        // Reactions on their own are never rendered.
                        if (te?.isReaction() == true) {
                            val relatedEventId = te.getRelatedEventId()

                            if (relatedEventId != null) {
                                val relatedI = oldIds.indexOf(relatedEventId)

                                if (relatedI > -1) notifyItemChanged(relatedI)
                            }
                        } else {
                            notifyItemInserted(i)
                        }
                    } else if (oldI != i) {
                        notifyItemMoved(oldI, i)
                    }
                }


                //If at bottom, then jump to new message.
                if (mIsBackPage && mTimelineIds.size > 20) {
                    jumpToPosition(20)
                } else if (mView.canScrollVertically(1) == false) {
                    jumpToFirstUnread()
                }

                 if (mTimeline?.hasMoreToLoad(Timeline.Direction.BACKWARDS) == true)
                    mTimeline?.paginate(Timeline.Direction.BACKWARDS,100)


                mIsBackPage = false
            }
        }

        fun jumpToPosition(position: Int) {
            val count = itemCount ?: 0

            if (count > 0) {

                if (position > 0) {
                    mView.layoutManager?.scrollToPosition(position - 1)
                }
                else {
                    mView.layoutManager?.scrollToPosition(0)
                }
            }
        }

        override fun onTimelineFailure(throwable: Throwable) {
         //   Timber.d(throwable)
        }

        override fun onNewTimelineEvents(eventIds: List<String>) {
            // No reason to notify here, since onTimelineUpdated is always called right after this.

            mHandler.post {
                for (eventId in eventIds) {
                    trackProgressIfOngoingUpload(eventId)
                }
            }
        }

        private fun jumpToFirstUnread() {

            var firstUnread = 0

            for (i in mTimelineIds.indices) {
                if (mRoom?.isEventRead(mTimelineIds[i]) == true) {
                    break
                } else {
                    firstUnread = i
                }
            }

            // #isEventRead seems to be continuously off by one.
            firstUnread = (firstUnread - 1).coerceAtLeast(0)

            // List is displayed inverted! (Position 0 at bottom!)
           // mView.jumpToPosition(mTimelineIds.size - firstUnread)

            if (mTimelineIds.isEmpty()) return

            mCoroutineScope.launch {
                mTimeline?.getTimelineEventAtIndex(firstUnread)?.eventId?.let {
                    mRoom?.setReadMarker(
                        it
                    )
                }
            }

        }

        private fun trackProgressIfOngoingUpload(eventId: String) {
            val te = mTimeline?.getTimelineEventWithId(eventId)

            if ((te?.eventId?.let { LocalEcho.isLocalEchoId(it) } == true
                        && te.root.senderId == mSession?.myUserId)
                && te.root.isAttachmentMessage()
            ) {
                mSession?.contentUploadProgressTracker()?.track(te.eventId, this@ResponseMessageAdapter)
            }
        }
    }
    private var quickResponseLongClick: QuickResponseLongClickListener? = null


    init {
        setHasStableIds(true)
        this.quickResponseLongClick = quickResponseLongClick
        startListening()
    }

    private fun startListening() {
        mTimeline?.addListener(tListener)
        mTimeline?.start()
    }

    fun stopListening() {
        mTimeline?.removeListener(tListener)
    }

    fun dispose () {
        mTimeline?.dispose()
    }

    fun pageBack() {
        if (mTimeline?.hasMoreToLoad(Timeline.Direction.BACKWARDS) == true) {
            mIsBackPage = true
            mTimeline.paginate(Timeline.Direction.BACKWARDS, 20)
        }
    }

    override fun getItemId(position: Int): Long {
        return getTimelineEvent(position)?.localId ?: 0
    }

    override fun getItemCount(): Int {
        return mTimelineIds.size
    }

    enum class ViewType {
        INBOUND_MESSAGE, OUTBOUND_MESSAGE, STATUS, VERIFICATION_REQUEST
    }

    override fun getItemViewType(position: Int): Int {
        val event = getTimelineEvent(position)

        return when (event?.root?.type) {
            EventType.MESSAGE, EventType.ENCRYPTED -> {
                when {
                    event.getLastMessageContent()?.msgType == MessageType.MSGTYPE_VERIFICATION_REQUEST ->
                        ViewType.VERIFICATION_REQUEST.ordinal

                    event.root.senderId != mSession?.myUserId -> ViewType.INBOUND_MESSAGE.ordinal

                    else -> ViewType.OUTBOUND_MESSAGE.ordinal
                }
            }

            else -> ViewType.STATUS.ordinal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val vh = when (ViewType.values()[viewType]) {

            ViewType.INBOUND_MESSAGE -> {
                ResponseViewHolder(StatusReplyBaseBinding.inflate(inflater, parent, false))
            }

            ViewType.OUTBOUND_MESSAGE -> {
                ResponseViewHolder(StatusReplyBaseBinding.inflate(inflater, parent, false))
                //MessageViewHolder(MessageViewRightBinding.inflate(inflater, parent, false))
            }

            ViewType.VERIFICATION_REQUEST -> {
                VerificationRequestItemHolder(
                    VerificationRequestItemBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }

            else -> {
                StatusItemHolder(StatusItemBinding.inflate(inflater, parent, false))
            }
        }

        if (vh is ResponseViewHolder) {
            vh.onImageClickedListener = this
        }

        return vh
    }

    private val mThumbnails = HashMap<Int, MediaInfo>()

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {

        val te = getTimelineEvent(position)
        val mc = te?.getLastMessageContent()
        var attachment: Uri? = null
        var thumbnail: Uri? = null

        if (mc is MessageWithAttachmentContent) {
            // Trigger download and decryption, if not available, yet.
            if (!mDownloadedDecryptedFiles.containsKey(position) && mc.getFileUrl()
                    ?.isNotEmpty() == true
            ) {
                val fileState = mSession?.fileService()?.fileState(mc)

                var fileName: String = mc.getFileName()
                var url: String? = null
                var mimeType: String? = null
                var info: EncryptedFileInfo? = null

                if (fileState != FileService.FileState.Downloading) {
                    mCoroutineScope.launch {
                        mimeType = mc.mimeType
                        url = mc.getFileUrl()
                        info = mc.encryptedFileInfo

                        if (url?.startsWith("content") == true) {
                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.parse(url), mimeType)

                            mView?.post {
                                notifyItemChanged(position)
                            }
                        } else if (url?.startsWith("mxc") == true) {
                            val fileMedia = mSession?.fileService()?.downloadFile(
                                fileName, mimeType, url, info?.toElementToDecrypt()
                            )

                            mDownloadedDecryptedFiles[position] =
                                MediaInfo(Uri.fromFile(fileMedia), mimeType)

                            mView?.post {
                                notifyItemChanged(position, fileMedia)
                            }
                        }
                    }
                }

                when (mc) {
                    is MessageImageContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageStickerContent -> {
                        url = mc.info?.thumbnailUrl ?: mc.info?.thumbnailFile?.url
                        mimeType = mc.info?.thumbnailInfo?.mimeType
                        info = mc.info?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }

                    is MessageVideoContent -> {
                        url = mc.videoInfo?.thumbnailUrl ?: mc.videoInfo?.thumbnailFile?.url
                        mimeType = mc.videoInfo?.thumbnailInfo?.mimeType
                        info = mc.videoInfo?.thumbnailFile
                        fileName = "thumb" + mc.getFileName()
                    }
                    is MessageAudioContent -> {

                        url = mc.getFileUrl()
                        mimeType = mc.audioInfo?.mimeType
                        info = mc.encryptedFileInfo
                        fileName = "thumb" + mc.getFileName()

                    }
                    is MessageFileContent -> {

                        if (mc.info?.mimeType?.contains("video", true) == true
                        ) {
                            url = mc.url
                            fileName = "thumb" + mc.getFileName()
                        } else if (mc.info?.mimeType?.contains("octet-stream", true) == true) {
                            url = mc.url
                        }

                    }
                }

                if (url?.isNotEmpty() == true) {
                    val fileThumbState = mSession?.fileService()
                        ?.fileState(url, mc.getFileName(), mimeType, info?.toElementToDecrypt())

                    if (fileThumbState != FileService.FileState.Downloading) {
                        mCoroutineScope.launch {
                            if (url?.startsWith("content") == true) {
                                mThumbnails[position] = MediaInfo(Uri.parse(url), mimeType)

                                mView?.post {
                                    notifyItemChanged(position)
                                }
                            } else if (url?.startsWith("mxc") == true) {
                                val fileMedia = mSession?.fileService()?.downloadFile(
                                    fileName,
                                    mimeType,
                                    url,
                                    info?.toElementToDecrypt()
                                )

                                mThumbnails[position] = MediaInfo(Uri.fromFile(fileMedia), mimeType)

                                mView?.post {
                                    notifyItemChanged(position, fileMedia)
                                }
                            }
                        }
                    }
                }
            } else {
                attachment = mDownloadedDecryptedFiles[position]?.uri
                thumbnail = mThumbnails[position]?.uri
            }
        }

        val reactions = HashMap<String, QuickReaction>()

        for (reaction in te?.annotations?.reactionsSummary ?: emptyList()) {
            val qr = reactions[reaction.key] ?: QuickReaction(reaction.key, ArrayList())
            if (reaction.addedByMe) qr.sentByMe = true

            for (eventId in reaction.sourceEvents) {
                mRoom?.getTimeLineEvent(eventId)?.senderInfo?.userId?.let {
                    qr.senders.add(it)
                }
            }

            reactions[reaction.key] = qr
        }

        val event = te?.root
        val eventId = event?.eventId

        eventId?.let {
            mCoroutineScope.launch {
                mRoom?.setReadMarker(it)
            }

            if (te.readReceipts.isEmpty()) {
                mCoroutineScope.launch {
                    mRoom?.setReadReceipt(it)
                }
            }
        }

        if (event?.isEncrypted() == true) {
            val timelineId = mTimeline?.timelineID
            val messageMap = event.getClearContent()

            if (timelineId != null && (messageMap?.get("body") as? String).isNullOrEmpty()) {
                mSession?.cryptoService()?.decryptEventAsync(
                    event,
                    timelineId,
                    object : MatrixCallback<MXEventDecryptionResult> {

                        override fun onSuccess(data: MXEventDecryptionResult) {
                            mView?.post {
                                notifyItemChanged(
                                    position
                                )
                            }
                        }

                        override fun onFailure(failure: Throwable) {
                            //Timber.d(failure)
                        }
                    })
            }
        }

        when (viewHolder) {
            is ResponseViewHolder -> {
                val isIncoming = event?.senderId != mSession?.myUserId

                val contextMenuView = when {

                    viewHolder.container != null -> viewHolder.container

                    else -> viewHolder.itemView
                }

                /**
                contextMenuView?.setOnCreateContextMenuListener { menu: ContextMenu, _: View?, _: ContextMenuInfo? ->
                    mActivity.menuInflater.inflate(R.menu.menu_message_avatar, menu)


                    menu.findItem(R.id.menu_message_add_reaction).setOnMenuItemClickListener {
                        // Pick emoji as quick reaction.
                        contextMenuView.post { mView.showQuickReactionsPopup(eventId) }

                        true
                    }
                }**/

                if (isIncoming) {
                    contextMenuView?.setOnClickListener {
                        mActivity.startActivity(
                            mApp?.router?.friend(
                                mActivity,
                                te?.senderInfo?.userId
                            )
                        )
                    }
                }

                contextMenuView?.setOnLongClickListener {
                    if (te != null) {
                        quickResponseLongClick?.onQuickResponseClick(
                            timeline = te,
                            attachment,
                            thumbnail
                        )
                    }
                    true
                }

                if (te != null) {

                    viewHolder.bind(
                        te, null, attachment, thumbnail, reactions.values.toList(),
                        mRoom?.isEncrypted() == true
                    )
                }
            }

            is VerificationRequestItemHolder -> {
                if (te != null) {
                    viewHolder.bind(te)
                }
            }

            is StatusItemHolder -> {
                if (te != null) {
                    viewHolder.bind(te)
                }
            }
        }
    }

    override fun onImageClicked(image: Uri) {
        val uris = ArrayList<Uri>()
        val mimeTypes = ArrayList<String>()

        mDownloadedDecryptedFiles.filter { it.value.isImage }.keys.sortedDescending()
            .forEach { i ->
                mDownloadedDecryptedFiles[i]?.let { it ->
                    uris.add(it.uri)
                    mimeTypes.add(it.mimeType)
                }
            }

        mCoroutineScope.launch {
            mApp?.router?.imageView(
                mActivity, uris, mimeTypes,
                0.coerceAtLeast(uris.indexOf(image)), showResend = true
            )
        }

    }

    /**
    override fun onQuickReactionClicked(quickReaction: QuickReaction, eventId: String) {
    // TODO - Remove my own reaction, but that is just sending it twice right?
    mView.sendQuickReaction(quickReaction.reaction, eventId)
    }**/

    override fun onUpdate(state: ContentUploadStateTracker.State) {
        when (state) {
            ContentUploadStateTracker.State.Idle -> {
                unsetProgress()
            }

            ContentUploadStateTracker.State.EncryptingThumbnail -> {
                setProgress()
            }

            ContentUploadStateTracker.State.CompressingImage -> {
                setProgress()
            }

            is ContentUploadStateTracker.State.CompressingVideo -> {
                setProgress((state.percent * 10).toLong(), 1000)
            }

            is ContentUploadStateTracker.State.UploadingThumbnail -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Encrypting -> {
                setProgress(state.current, state.total)
            }

            is ContentUploadStateTracker.State.Uploading -> {
                setProgress(state.current, state.total)
            }

            ContentUploadStateTracker.State.Success -> {
                unsetProgress()
            }

            is ContentUploadStateTracker.State.Failure -> {
                mActivity?.let {
                    Snackbar.make(
                        mView,
                        mActivity.getString(R.string.error_prefix) + state.throwable.betterMessage,
                        Snackbar.LENGTH_LONG
                    )
                        .show()
                }

                unsetProgress()
            }
        }
    }

    private fun setProgress(current: Long = 0, total: Long = 0) {
        val pos = mShowProgressOnPosition ?: return

        mProgressCurrent = current
        mProgressTotal = total

        notifyItemChanged(pos)
    }

    private fun unsetProgress() {
        val pos = mShowProgressOnPosition ?: return

        mShowProgressOnPosition = null

        getTimelineEvent(pos)?.eventId?.let {
            mSession?.contentUploadProgressTracker()?.untrack(it, this)
        }

        notifyItemChanged(pos)
    }

    /**
     * [TimelineEvent]s are stored upside-down: newest first, but displayed newest at bottom.
     *
     * This will give back the correct [TimelineEvent] for a given [RecyclerView] position.
     *
     * @return
     *      the correct [TimelineEvent] for the given position.
     */
    fun getTimelineEvent(position: Int): TimelineEvent? {
        // Don't use `mTimeline?.getTimelineEventWithId(eventId)`, it won't show unsent events!
        return mRoom?.getTimeLineEvent(mTimelineIds[position])
    }

    fun redactAll () {
        for (i in mTimelineIds.size-1 downTo 0) {
            val te = getTimelineEvent(i)
            mRoom?.redactEvent(te?.root!!, "resolved")
        }
    }
}


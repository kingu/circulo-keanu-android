package org.article19.circulo.next.main.updatestatus

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.main.now.People
import org.article19.circulo.next.main.now.PeopleAdapter
import org.article19.circulo.next.main.now.Status
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageBody
import android.content.Intent




class StatusDetailActivity : BaseActivity() {

    private lateinit var tvStatus: TextView
    private lateinit var rvAdapter: ResponseMessageAdapter


    private val mApp: ImApp?
        get() = application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mEvent: TimelineEvent? = null
    private var mRoom: Room? = null

    private var mLocation = ""

    private lateinit var mToolbar : Toolbar

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mApp?.matrixSession?.startSync(true)

        setContentView(R.layout.activity_status_detail)

        mToolbar = findViewById<Toolbar>(R.id.toolbar)
        mToolbar.findViewById<ImageView>(R.id.iv_back).setOnClickListener {
            finish()
        }

        val etStatus = findViewById<EditText>(R.id.et_update_status)
        etStatus.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addStatusReply(etStatus.text.toString())
                etStatus.text.clear()
                tvStatus.requestFocus()
                findViewById<View>(R.id.reply_options).visibility = View.VISIBLE
                true
            }
            else
            {
                findViewById<View>(R.id.reply_options).visibility = View.GONE
            }
            false
        }
        etStatus.setOnFocusChangeListener { v, hasFocus ->

            if (hasFocus)
                findViewById<View>(R.id.reply_options).visibility = View.GONE
            else
                findViewById<View>(R.id.reply_options).visibility = View.VISIBLE

        }

        findViewById<View>(R.id.layout_location).setOnClickListener {
            if (mLocation.isNotEmpty())
            {
                showMap()
            }
        }

        val rvReplies = findViewById<RecyclerView>(R.id.rv_replies)
        val llm = WrapContentLinearLayoutManager(this)
        rvReplies.layoutManager = llm

        val personStatus =
            intent.getParcelableExtra<People>(PeopleAdapter.BUNDLE_EXTRA_PERSON_STATUS)

            if (personStatus?.eventId != null) {

                mRoom = mSession?.getRoom(personStatus?.roomId)
                mEvent = mRoom?.getTimeLineEvent(personStatus?.eventId)

                if (mRoom != null) {
                    mApp?.currentForegroundRoom = mRoom?.roomId.toString()
                }

                val senderIsMe = mEvent?.senderInfo?.userId.equals(mSession?.myUserId)

                if (senderIsMe) {
                    val viewResolve = mToolbar.findViewById<View>(R.id.tv_resolve_status)
                    viewResolve.visibility = View.VISIBLE
                    viewResolve.setOnClickListener {
                        if (mEvent?.root != null) {
                            mRoom?.redactEvent(mEvent?.root!!, "resolved")
                            rvAdapter?.redactAll()
                        }

                        finish()
                    }
                }


                when (personStatus?.status) {
                    Status.SAFE -> {
                        mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_safe)
                        mToolbar.findViewById<TextView>(R.id.tv_status).text =
                            getString(R.string.safe)

                    }
                    Status.UNCERTAIN -> {
                        mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_uncertain)
                        mToolbar.findViewById<TextView>(R.id.tv_status).text =
                            getString(R.string.uncertain)
                    }
                    Status.NOT_SAFE -> {
                        mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_not_safe)
                        mToolbar.findViewById<TextView>(R.id.tv_status).text =
                            getString(R.string.not_safe)
                    }

                }

                when (personStatus?.isUrgent) {
                    true -> {
                        mToolbar.findViewById<TextView>(R.id.tv_urgent).visibility = View.VISIBLE
                    }
                    else -> {
                        mToolbar.findViewById<TextView>(R.id.tv_urgent).visibility = View.GONE
                    }
                }

                mToolbar.findViewById<TextView>(R.id.tv_author_name).text = personStatus?.name
                tvStatus = findViewById(R.id.tv_status)

                mCoroutineScope.launch {
                    val user = mEvent?.senderInfo?.userId?.let { mApp?.matrixSession?.getUser(it) }
                    if (user?.avatarUrl?.isNotEmpty() == true) {

                        mSession?.contentUrlResolver()?.resolveThumbnail(
                            user.avatarUrl,
                            48,
                            48,
                            ContentUrlResolver.ThumbnailMethod.SCALE
                        )
                            ?.let {
                                withContext(Dispatchers.Main) {
                                    GlideUtils.loadImageFromUri(this@StatusDetailActivity, Uri.parse(it), mToolbar.findViewById(R.id.iv_avatar_author), false)
                                }
                            }

                    }
                }

                val tvMessage = findViewById<TextView>(R.id.tv_message)

                tvMessage.text = processStatus(mEvent?.getLastMessageBody()?.toString())

                findViewById<ImageView>(R.id.iv_choose_location).setOnClickListener {

                }

                if (senderIsMe) {
                    findViewById<ImageView>(R.id.iv_conditions).setOnClickListener {
                        showMyConditionDialog()
                    }
                }
                else
                {
                    findViewById<ImageView>(R.id.iv_conditions).visibility = View.GONE
                }

                findViewById<ImageView>(R.id.iv_voice_msg).setOnClickListener {

                }

                this@StatusDetailActivity.runOnUiThread(java.lang.Runnable {

                    rvAdapter = ResponseMessageAdapter(this@StatusDetailActivity, mRoom?.roomId!!,
                        mEvent?.eventId!!,rvReplies
                    )
                    rvReplies.adapter = rvAdapter


                })



            } else
                finish()

    }

    fun updateStatusFromString (status: String?) : Boolean {

        if (status?.contains(Status.SAFE_STRING) == true) {
            mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_safe)
            mToolbar.findViewById<TextView>(R.id.tv_status).text =
                getString(R.string.safe)

            return true
        }
        else if (status?.contains(Status.NOT_SAFE_STRING) == true) {

            mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_not_safe)
            mToolbar.findViewById<TextView>(R.id.tv_status).text =
                getString(R.string.not_safe)

            return true
        }
        else if (status?.contains(Status.UNCERTAIN_STRING) == true) {
            mToolbar.setBackgroundResource(R.drawable.bg_toolbar_status_detail_uncertain)
            mToolbar.findViewById<TextView>(R.id.tv_status).text =
                getString(R.string.uncertain)

            return true
        }

        return false
    }

    private fun processStatus (status: String?): String {
        val pStatus = StringBuffer()

        val statusParts = status?.split(" ")

        if (statusParts != null) {
            for (statusPart in statusParts)
            {
                if (statusPart.startsWith("#"))
                {
                    if (statusPart.startsWith("#geo"))
                    {
                        mLocation = statusPart.substring(1)
                    }
                    else
                    {
                        //hide it
                    }
                }
                else
                {
                    pStatus.append(statusPart).append(" ")
                }
            }
        }

        return pStatus.toString().trim()
    }

    private fun showMyConditionDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_my_condition)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.findViewById<RelativeLayout>(R.id.layout_not_safe)
            .setOnClickListener {
                tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.NOT_SAFE_STRING)
                dialog.dismiss()
            }

        dialog.findViewById<RelativeLayout>(R.id.layout_uncertain)
            .setOnClickListener {
                tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.UNCERTAIN_STRING)

                dialog.dismiss()
            }

        dialog.findViewById<RelativeLayout>(R.id.layout_safe)
            .setOnClickListener {
                tvStatus.visibility = View.VISIBLE
                addStatusReply(Status.SAFE_STRING)
                dialog.dismiss()
            }

        dialog.show()

    }

    private fun showMap ()
    {

        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(mLocation)
        )
        startActivity(intent)

    }

    private fun addStatusReply (update : String)
    {
        if (mEvent != null && mEvent?.roomId != null) {

            mRoom?.replyToMessage(mEvent!!, update)

        }
    }

    override fun onDestroy() {
        super.onDestroy()

        rvAdapter.stopListening()
        rvAdapter.dispose()
    }

    override fun onPause() {
        super.onPause()
        mApp?.currentForegroundRoom = ""
    }

    override fun onResume() {
        super.onResume()
        if (mRoom != null) {
            mApp?.currentForegroundRoom = mRoom?.roomId.toString()
        }
    }
}

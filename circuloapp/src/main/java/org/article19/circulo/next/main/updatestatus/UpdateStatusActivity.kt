package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.RECORD_AUDIO
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.media.AudioManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.Window
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.tougee.recorderview.AudioRecordView
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.ui.room.RoomActivity.Companion.REQUEST_ADD_MEDIA
import info.guardianproject.keanu.core.util.AttachmentHelper
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.main.now.Status
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.w3c.dom.Text
import timber.log.Timber
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class UpdateStatusActivity : BaseActivity(), AudioRecordView.Callback {

    private lateinit var tvStatus: TextView
    private lateinit var firstQuickResponseList: List<String>
    private lateinit var secondQuickResponseList: List<String>
    private lateinit var quickResponseAdapter: QuickResponseAdapter


    private lateinit var mMediaRecorder : MediaRecorder

    private var condition = -1
    private var urgent = false
    private lateinit var location : Location

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private lateinit var mRoomSummary : RoomSummary


    private lateinit var mAudioFilePath : File
    private var isAudioRecording = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_update_status)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<ImageView>(R.id.iv_back).setOnClickListener {
            finish()
        }

        tvStatus = findViewById(R.id.tv_status)
        findViewById<ImageView>(R.id.iv_choose_location).setOnClickListener {

            addLocation()
        }

        findViewById<ImageView>(R.id.iv_conditions).setOnClickListener {
            showMyConditionDialog()
        }

        findViewById<ImageView>(R.id.iv_voice_msg).setOnClickListener {

           activateVoiceMessage()
        }

        findViewById<View>(R.id.iv_add_media).setOnClickListener {
            startImagePicker()
        }


        findViewById<View>(R.id.tv_send_to_circle).setOnClickListener {
            sendStatus()
            finish()
        }

        val recyclerViewQuickResponse = findViewById<RecyclerView>(R.id.recycler_view_suggested_keywords)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewQuickResponse.layoutManager = layoutManager

        firstQuickResponseList = ArrayList(resources.getStringArray(R.array.quick_response_1).toMutableList())
        quickResponseAdapter = QuickResponseAdapter(firstQuickResponseList)
        recyclerViewQuickResponse.adapter = quickResponseAdapter

        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        var queryParams = builder.build()
        var listRooms = mSession?.getRoomSummaries(queryParams)
        mRoomSummary = listRooms?.get(0)!!
    }

    private fun showMyConditionDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_my_condition)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.findViewById<RelativeLayout>(R.id.layout_not_safe)
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.NOT_SAFE,dialog.findViewById<CheckBox>(R.id.check_box_help_now).isChecked)
            }

        dialog.findViewById<RelativeLayout>(R.id.layout_uncertain)
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.UNCERTAIN,dialog.findViewById<CheckBox>(R.id.check_box_help_now).isChecked)
            }

        dialog.findViewById<RelativeLayout>(R.id.layout_safe)
            .setOnClickListener {
                dialog.dismiss()
                setCondition (Status.SAFE,dialog.findViewById<CheckBox>(R.id.check_box_help_now).isChecked)
            }

        dialog.show()

    }

    private fun appendText (appendStatus: String) {
        findViewById<EditText>(R.id.edit_text_status).text.append("$appendStatus ")

    }

    fun updateQuickResponseList(selectedResponse: String) {
        if (firstQuickResponseList.contains(selectedResponse)) {
            secondQuickResponseList = ArrayList(resources.getStringArray(R.array.quick_response_2).toMutableList())
            quickResponseAdapter.replaceData(secondQuickResponseList)
        }

        appendText("$selectedResponse ")

    }

    private fun activateVoiceMessage () {

        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(android.Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else
        {
            var arView = findViewById<AudioRecordView>(R.id.record_view)
            arView.visibility = View.VISIBLE
            arView.activity = this
            arView.callback = this

        }

    }


    private var requestCodeLocation = 9999
    private var requestCodeAudio = 9998

    private fun addLocation () {

        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocation)
        }
        else {

            LocationUtils().getLocation(this)?.observe(this, Observer {loc: Location? ->
                location = loc!!
                // Yay! location recived. Do location related work here
                //Log.i(TAG,"Location: ${location.latitude}  ${location.longitude}")
                findViewById<View>(R.id.layout_location).visibility = View.VISIBLE

            })

        }
    }


    private fun setCondition (newCondition : Int, newUrgent: Boolean)
    {
        condition = newCondition
        urgent = newUrgent

        var tvStatus = findViewById<TextView>(R.id.tv_status)

        tvStatus.visibility = View.VISIBLE
        if (condition == Status.NOT_SAFE) {
            tvStatus.setText(R.string.status_set_to_not_safe)
            tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_not_safe_round, 0, 0, 0);
        }
        else if (condition == Status.UNCERTAIN) {
            tvStatus.setText(R.string.status_set_to_uncertain)
            tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_uncertain_round, 0, 0, 0);
        }
        else if (condition == Status.SAFE) {
            tvStatus.setText(R.string.status_set_to_safe)
            tvStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_status_safe_round, 0, 0, 0);
        }





    }


    private fun sendStatus () {

        var statusText = StringBuffer()

        statusText.append(findViewById<EditText>(R.id.edit_text_status).text.toString())

        if (this::location.isInitialized)
        {
            //Log.i(TAG,"Location: ${location.latitude}  ${location.longitude}")

            statusText.append(" ").append("#geo:${location.latitude},${location.longitude}")
        }

        if (condition == Status.NOT_SAFE)
            statusText.append(" ").append(Status.NOT_SAFE_STRING)
        else if (condition == Status.UNCERTAIN)
            statusText.append(" ").append(Status.UNCERTAIN)
        else if (condition == Status.SAFE)
            statusText.append(" ").append(Status.SAFE_STRING)

        if (urgent)
            statusText.append(" ").append(Status.URGENT_STRING)

        if (statusText.isNotEmpty())
            mSession?.getRoom(mRoomSummary?.roomId)?.sendTextMessage(statusText)

        sendMediaAttachment()
        sendAudioStatus()
    }

    class LocationUtils{

        private var fusedLocationProviderClient: FusedLocationProviderClient ?= null
        private var location : MutableLiveData<Location> = MutableLiveData()

        // using singleton pattern to get the locationProviderClient
        fun getInstance(appContext: Context): FusedLocationProviderClient{
            if(fusedLocationProviderClient == null)
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(appContext)
            return fusedLocationProviderClient!!
        }

        fun getLocation(context: Context) : LiveData<Location>? {

            var client = getInstance(context)

            if (client != null) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return null
                }
                client!!.lastLocation
                    .addOnSuccessListener { loc: Location? ->
                        location.value = loc

                    }
            }

            return location
        }

    }

    override fun isReady(): Boolean {
        return true
    }

    override fun onRecordCancel() {
        stopAudioRecording(false)
    }

    override fun onRecordEnd() {


        stopAudioRecording(true)

        findViewById<View>(R.id.layout_voice_message).visibility = View.VISIBLE

    }

    override fun onRecordStart(audio: Boolean) {

        startAudioRecording()
    }

    private val mStartAudioRecordingIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startAudioRecording()
        }


    private fun startAudioRecording() {
        if (ActivityCompat.checkSelfPermission(
                this,
                RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, RECORD_AUDIO)) {
                Snackbar.make(findViewById(R.id.updateroot), R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startAudioRecording()
                    }
                    .show()
            } else {
                mStartAudioRecordingIfPermitted.launch(RECORD_AUDIO)
            }
        } else {
            val am = getSystemService(AUDIO_SERVICE) as? AudioManager

            if (am?.mode == AudioManager.MODE_NORMAL) {
                val fileName = UUID.randomUUID().toString().substring(0, 8) + ".m4a"
                mAudioFilePath = File(filesDir, fileName)

                mMediaRecorder = MediaRecorder()
                mMediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                mMediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                mMediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                // Maybe we can modify these in the future, or allow people to tweak them.
                mMediaRecorder?.setAudioChannels(1)
                mMediaRecorder?.setAudioEncodingBitRate(44100 * 16)
                mMediaRecorder?.setAudioSamplingRate(44100)
                mMediaRecorder?.setOutputFile(mAudioFilePath!!.absolutePath)

                try {
                    isAudioRecording = true
                    mMediaRecorder?.prepare()
                    mMediaRecorder?.start()
                } catch (e: Exception) {
                   Timber.e(e, "couldn't start audio")
                }
            }
        }
    }

    private fun stopAudioRecording(send: Boolean) {
        if (mMediaRecorder != null && mAudioFilePath != null && isAudioRecording) {
            try {
                mMediaRecorder?.stop()
                mMediaRecorder?.reset()
                mMediaRecorder?.release()

                if (send) {

                } else {
                    mAudioFilePath?.delete()
                }
            } catch (ise: IllegalStateException) {
                Timber.w(ise, "error stopping audio recording")
            } catch (re: RuntimeException) {
                // Stop can fail so we should catch this here.

                Timber.w(re, "error stopping audio recording")
            }

            isAudioRecording = false
        }
    }

    private fun sendMediaAttachment () {
        if (this::mMediaUri.isInitialized) {
            sendAttachment(mMediaUri, null)
        }
    }

    private fun sendAudioStatus () {
        if (this::mAudioFilePath.isInitialized) {
            val uriAudio = Uri.fromFile(mAudioFilePath)
            sendAttachment(uriAudio, "audio/x-m4a")
        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null) {
        try {
            var mimeType = fallbackMimeType
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString())
                )
            }

            val attachment =
                AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

            mApp?.matrixSession?.getRoom(mRoomSummary?.roomId)?.sendMedia(
                attachment,
                true, setOf(mRoomSummary?.roomId)
            )

        } catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    private val mStartImagePickerIfPermitted =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) startImagePicker()
        }

    private fun startImagePicker() {
        if (ActivityCompat.checkSelfPermission(
                this,
                READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    READ_EXTERNAL_STORAGE
                )
            ) {
                Snackbar.make(findViewById(R.id.updateroot), R.string.grant_perms, Snackbar.LENGTH_LONG)
                    .setAction(R.string.ok) {
                        startImagePicker()
                    }
                    .show()
            } else {
                mStartImagePickerIfPermitted.launch(READ_EXTERNAL_STORAGE)
            }
        } else {
            Matisse.from(this)
                .choose(MimeType.ofAll())
                .countable(true)
                .maxSelectable(100)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .thumbnailScale(0.85f)
                .imageEngine(GlideEngine())
                .showPreview(false)
                .forResult(REQUEST_ADD_MEDIA)
        }
    }

    private lateinit var mMediaUri : Uri
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                mMediaUri = mediaUri
            }
        } else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


}
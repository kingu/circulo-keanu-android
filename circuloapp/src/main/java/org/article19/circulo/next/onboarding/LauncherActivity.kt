package org.article19.circulo.next.onboarding

import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import androidx.fragment.app.FragmentActivity
import org.article19.circulo.next.R

class LauncherActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_launcher)

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            this.window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }


        val welcomeFragment = WelcomeFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frame_root, welcomeFragment).commit()
    }

    fun openCreateProfileView() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_root, CreateProfileFragment())
            .addToBackStack("welcome")
        fragmentTransaction.commit()
    }
}
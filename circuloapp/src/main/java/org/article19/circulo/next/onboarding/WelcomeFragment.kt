package org.article19.circulo.next.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.ui.auth.AuthenticationActivity
import info.guardianproject.keanu.core.ui.onboarding.OnboardingAccount
import info.guardianproject.keanuapp.ui.BaseActivity
import org.article19.circulo.next.CirculoRouter
import org.article19.circulo.next.R

class WelcomeFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_welcome, container, false)
        rootView.findViewById<Button>(R.id.btn_get_started).setOnClickListener {
            (activity as LauncherActivity).openCreateProfileView()
        }

        rootView.findViewById<View>(R.id.btn_login).setOnClickListener() {
            mRequestSignInUp.launch(activity?.let { it1 -> CirculoRouter.instance.authentication(it1) })

        }

        return rootView
    }

    private var mRequestSignInUp = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode != BaseActivity.RESULT_OK) return@registerForActivityResult

        result.data?.getParcelableExtra<OnboardingAccount>(AuthenticationActivity.EXTRA_ONBOARDING_ACCOUNT)?.let {
            showMainScreen(it.isNew)
        }
    }

    private fun showMainScreen(isNewAccount: Boolean) {
        startActivity(CirculoRouter.instance.main(requireActivity(), isFirstTime = isNewAccount))

        requireActivity().finish()
    }

}
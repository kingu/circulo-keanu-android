package org.article19.circulo.next.profile

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.ImageChooserHelper
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.main.MainActivity
import java.io.File


open class BaseProfileFragment : Fragment() {

    private val TAG = "BaseProfileFragment"

    lateinit var viewBinding: View
    lateinit var editTextName: EditText
    lateinit var toolbar: Toolbar
    lateinit var tvActionDone: TextView
    lateinit var ivAvatar: RoundRectCornerImageView

    private var mCurrentSelectedAvatarPos = 0

    private val MAX_DEFAULT_AVATAR_COUNT = 16

    private var mIsCustomAvatarSelected = false
    var isAvatarChanged = false
    lateinit var customAvatarBitmap: Bitmap

    private val mDefaultAvatars by lazy {
        arrayOf(R.drawable.ic_avatar_1, R.drawable.ic_avatar_2,
            R.drawable.ic_avatar_3, R.drawable.ic_avatar_4,
            R.drawable.ic_avatar_5, R.drawable.ic_avatar_6,
            R.drawable.ic_avatar_7, R.drawable.ic_avatar_8,
            R.drawable.ic_avatar_9, R.drawable.ic_avatar_10,
            R.drawable.ic_avatar_11, R.drawable.ic_avatar_12,
            R.drawable.ic_avatar_13, R.drawable.ic_avatar_14,
            R.drawable.ic_avatar_15, R.drawable.ic_avatar_16)
    }

    private lateinit var mPickAvatar: ActivityResultLauncher<Intent>

    val app: ImApp?
        get() = activity?.application as? ImApp

    val coroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(viewBinding.rootView))
    }

    private var mImageChooserHelper: ImageChooserHelper? = null

    private var mRequestCamera = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
    }

    lateinit var ivSelectDefaultAvatar: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activity = activity ?: return

        val helper = ImageChooserHelper(activity)
        mImageChooserHelper = helper

        mPickAvatar = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            helper.getCallback(this::setAvatar))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewBinding = inflater.inflate(R.layout.fragment_create_profile, container, false)


        toolbar = viewBinding.findViewById(R.id.toolbar)
        toolbar.findViewById<TextView>(R.id.tv_title).resources.getString(R.string.profile)

        toolbar.findViewById<TextView>(R.id.tv_back_text).setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
            showNavigationViewIfNeeded()
        }

        tvActionDone = toolbar.findViewById(R.id.tv_action)
        tvActionDone.visibility = View.VISIBLE
        tvActionDone.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
            showNavigationViewIfNeeded()
        }

        editTextName = viewBinding.findViewById(R.id.edit_text_choose_name)
        editTextName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.isNullOrBlank()) {
                    tvActionDone.isClickable = false
                    activity?.applicationContext?.let {
                        tvActionDone.setTextColor(
                            ContextCompat.getColor(
                                it,
                                R.color.holo_grey_dark
                            )
                        )
                    }
                } else {
                    tvActionDone.isClickable = true
                    activity?.applicationContext?.let {
                        tvActionDone.setTextColor(ContextCompat.getColor(it, R.color.app_primary))
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        if (activity is MainActivity) {
            (activity as MainActivity).hideNavigationView()
        }

        ivAvatar = viewBinding.findViewById(R.id.iv_avatar)

        ivSelectDefaultAvatar = viewBinding.findViewById(R.id.iv_select_default_avatar)
        ivSelectDefaultAvatar.setOnClickListener {
            selectDefaultAvatar()
        }

        viewBinding.findViewById<ImageView>(R.id.iv_avatar).setOnClickListener(View.OnClickListener {
            val activity = activity ?: return@OnClickListener

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                    Snackbar.make(it, info.guardianproject.keanuapp.R.string.grant_perms, Snackbar.LENGTH_LONG).show()
                } else {
                    mRequestCamera.launch(Manifest.permission.CAMERA)
                }
            } else {
                mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
            }
        })

        return viewBinding
    }

    fun selectDefaultAvatar() {
        if (mCurrentSelectedAvatarPos >= MAX_DEFAULT_AVATAR_COUNT) {
            mCurrentSelectedAvatarPos = 0
        }

        activity?.applicationContext?.let {
            val currentAvatar = mDefaultAvatars[mCurrentSelectedAvatarPos]
            ivAvatar.setImageDrawable(ContextCompat.getDrawable(it, currentAvatar))
            mCurrentSelectedAvatarPos++
            mIsCustomAvatarSelected = false
        }
    }

    private fun showNavigationViewIfNeeded() {
        if (activity is MainActivity) {
            (activity as MainActivity).showNavigationView()
        }
    }

    private fun setAvatar(bmp: Bitmap) {
        mIsCustomAvatarSelected = true
        ivAvatar.setImageDrawable(BitmapDrawable(resources, bmp))

        customAvatarBitmap = bmp

        if (this is EditProfileFragment) {
            coroutineScope.launch {
                updateAvatar(bmp)
            }
        }
    }

    suspend fun updateAvatar(avatarBitmap: Bitmap) {
        val userId = app?.matrixSession?.myUserId ?: return
        val file = File.createTempFile("avatar", ".jpg")

        file.outputStream().use { os ->
            avatarBitmap.compress(Bitmap.CompressFormat.JPEG, 90, os)
        }

        try {
            app?.matrixSession?.updateAvatar(userId, Uri.fromFile(file), file.name)
            isAvatarChanged = true
        }
        finally {
            file.delete()
        }
    }

    suspend fun uploadDefaultAvatarIfNeeded(): Boolean {
        return if (!mIsCustomAvatarSelected) {
            app?.applicationContext?.let {
                val currentSelectedAvatarPos =
                    if (mCurrentSelectedAvatarPos > 0) mCurrentSelectedAvatarPos -1
                    else mCurrentSelectedAvatarPos

                val selectedAvatarBitmap = getBitmapFromVectorDrawable(it, mDefaultAvatars[currentSelectedAvatarPos])
                selectedAvatarBitmap?.let {
                    updateAvatar(selectedAvatarBitmap)
                }
            }
            true
        } else {
            false
        }
    }

    open fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap? {
        var resultBitmap: Bitmap? = null
        try {
            val drawable = ContextCompat.getDrawable(context, drawableId)
            resultBitmap = Bitmap.createBitmap(
                drawable!!.intrinsicWidth,
                drawable!!.intrinsicHeight, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(resultBitmap)
            drawable?.setBounds(0, 0, canvas.width, canvas.height)
            drawable?.draw(canvas)

            return resultBitmap
        } catch (exception: Exception) {
            Log.e(TAG, "getBitmapFromVectorDrawable exception: " + exception)
        }

        return resultBitmap
    }
}